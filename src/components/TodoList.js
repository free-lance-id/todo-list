import Todo from './Todo';
import TodoForm from './TodoForm';
import TodoStatus from './TodoStatus';
import React, { useState } from 'react';

const TodoList = () => {
  const [todos, setTodos] = useState([]);
  const [totalTask, setTotalTask] = useState(0);
  const [todoIsComplete, setTodoIsComplete] = useState([]);


  //Add a task
  const addTodo = (todo) => {
    console.log('La valeur du todo dans le add', !todo.text)
    if (!todo.text) {
      return;
    }
    const newTodos = [todo, ...todos];
    setTodos(newTodos);
    setTotalTask(newTodos.length);
  };

  //remove a task
  const removeTodo = (id) => {
    let removedArr = [];
    let removedArrIsComplete = [];
    if(!isNaN(id)){
    removedArr = [...todos].filter((todo) => todo.id !== id);
    removedArrIsComplete = [...todoIsComplete].filter((todo) => todo.id !== id);
    setTodos(removedArr);
    setTotalTask(removedArr.length);
    setTodoIsComplete(removedArrIsComplete)
    } else {
      todoIsComplete.map(() => {
        removedArr = [...todos].filter((todo) => todo.isComplete === undefined);
        setTodos(removedArr);
        setTotalTask(removedArr.length);
        setTodoIsComplete([])
        return true;
      })
    }
  };

  //complete
  const completeTodo = (id) => {
    let updatedTodos = todos.map((todo) => {
      if (todo.id === id) {
        todo.isComplete = !todo.isComplete;
      }
      return todo;
    });
    setTodoIsComplete(updatedTodos.filter((todo) => todo.isComplete ===true));
    setTodos(updatedTodos);
  };

  const cleanDone = () => {
    if(todoIsComplete.length !==0) removeTodo(todoIsComplete);
  }

  return (
    <>
      <TodoForm 
        onSubmit={addTodo} 
      />
      <TodoStatus
        totalTask={totalTask}
        cleanDone={cleanDone}
        todoIsComplete={todoIsComplete}
        />
      <Todo
        todos={todos}
        removeTodo={removeTodo}
        completeTodo={completeTodo}
      />
    </>
  );
}

export default TodoList;
