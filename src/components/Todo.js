import React from 'react';
import { RiCloseCircleLine } from 'react-icons/ri';
import {TiInputCheckedOutline} from 'react-icons/ti'

const Todo = ({ todos, completeTodo, removeTodo }) => {

  return todos.map((todo, index) => (
    <div
      className={'todo-row'}
      key={index}
    >
      <div className='icons'>
        <RiCloseCircleLine
          className='delete-icon'
          onClick={() => removeTodo(todo.id)}
        />
        <TiInputCheckedOutline
          className='completed-icon'
          onClick={() => completeTodo(todo.id)}
        />
      </div>
      <div 
        className={todo.isComplete ? 'task complete' : 'task'}
        key={todo.id}>
        {todo.text}
      </div>
    </div>
  ));
};

export default Todo;
