import React from 'react';

const TodoStatus = ({totalTask=0, todoIsComplete=[], cleanDone}) => {
  return(
    <div className={'todo-status-row'}>
      <div className={'todo-status'}>
        <p>{todoIsComplete.length} </p>
        <span className={'separator'}> / </span>
        <p> {totalTask}</p>
      </div>
      <div>
        <button 
        className='clean-button'
        onClick={() => cleanDone()}>
            CLEAR DONE TODOS
        </button>
      </div>
  </div>
  )
}

export default TodoStatus;