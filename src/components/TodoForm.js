import React, { useState, useEffect, useRef } from 'react';

function TodoForm({edit=null, onSubmit}) {

  const [input, setInput] = useState(edit ? edit.value : '');

  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  });

  const handleChange = e => {
    setInput(e.target.value);
  };

  const handleSubmit = e => {
    e.preventDefault();
    onSubmit({
      id: Math.floor(Math.random() * 1000),
      text: input
    });

    //initialise the field
    setInput('');
  };

  return (
    <form onSubmit={handleSubmit} className='todo-form'>
        <>
          <input
            name='text'
            value={input}
            ref={inputRef}
            className='todo-input'
            onChange={handleChange}
            placeholder='Add a task to todo'
          />
          <button onClick={handleSubmit} className='todo-button'>
            Add todo
          </button>
        </>
      
    </form>
  );
}

export default TodoForm;
